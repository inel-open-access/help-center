function getParameterByName(name, url) {
             if (!url) url = window.location.href;
             name = name.replace(/[\[\]]/g, '\\$&');
             var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                 results = regex.exec(url);
             if (!results) return null;
             if (!results[2]) return '';
             return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

function analyzeHash(){
			/* analyze URL for hopping to requested subsection directly */
			var hash = window.location.hash.substr(1);

			  /* if hash is present in URL, that means a subsection is requested */
			if(null!=hash && hash!=''){

				 var tabButton = $('ul.nav-pills').children('li').children('a[href="#'+hash+'"]');

				 /* if hash refers to major topic (there must be an a[data-toggle=tab]) */
				 if(tabButton.length > 0){
				   /* close all tabs */
				   $('.tab-pane').removeClass('active in');

				   /* open major topic */
				   $(tabButton).tab('show');

				 }

				 /* if no major topic was referred to by hash, it must be a subtopic  */
				 else{
				   console.log('Hash: "'+hash+'"');
				   var requestedDiv = $('#'+hash);

				   /* open parent panel */
				   var parentPanel = $('#'+hash).parent();
				   console.log("parentPanel");
				   console.log(parentPanel);

				   if(!parentPanel.hasClass('active')){
					  $('ul.nav-pills').children('li').removeClass('active');
					  $('ul.nav-pills').children('li').has('a[href="#'+$(parentPanel).attr('id')+'"]').addClass('active');
					  $('.tab-pane').removeClass('active in');
					  parentPanel.addClass('active in');
				   }

				   /* collapse all other subsections */
				   $(parentPanel).children('div').each(function(i) {
					 var $this = $(this);
					 if(!$this.hasClass('panel-collapsed') && $this.attr('id')!=hash){
						$this.children('.panel-body').css("display", "none");
						$this.addClass('panel-collapsed');
						$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
					 }
				   });

				   /* expand requested entry */
				   $(requestedDiv).each(function(i) {
					 var $this = $(this);
					 if(!$this.hasClass('panel-collapsed')){
						$this.children('.panel-body').css("display", "block");
						$this.addClass('panel-collapsed');
						$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
					 }
				   });
				}
			}
		}

        $(document).ready( function( ) {

          /* analyze GET parameter for language, and set body's lang attribute accordingly */
          var language = getParameterByName('lang');
          document.querySelector('body').setAttribute('lang', language);
          document.querySelector('body').setAttribute('xml:lang', language);


          /* analyze URL for hash, and change display of subsections if present*/
	  analyzeHash();


          $(document).on('click', '.panel div.clickable', function (e) {
          
            console.log(this);
          
             var $this = $(this);
             if (!$this.hasClass('panel-collapsed')) {
			    /* collapse section */
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
             } else {
			    /* expand section */
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
             }
           });

           $(window).bind( 'hashchange', function(e) {
              analyzeHash();
           });
        });
